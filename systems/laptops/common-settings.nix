## This file contains the common settings for this category.
#! This file will overwrite the global settings!
{ # add updates below:

    system.type = "laptop";

    # overrides go here...

    # #| ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TESTING PROPER FUNCTIONING  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ |#
    # # This section is here to test if the settings are working, and are being overwritten properly.

    # TESTING_PROPER_FUNCTIONING = {
    #     SYSTEMS_LEVEL = {
    #         CATEGORY = "overriden by category and supposed to be overriden";
    #         MACHINE  = "overriden by category and supposed to be overriden again by machine";
    #     }; 
    #     CATEGORY_LEVEL = {
    #         CATEGORY = "not-overriden and not supposed to be overriden";  
    #         MACHINE  = "not-overriden and supposed to be overriden";
    #     };

    #     # The machine Should add their own section.
    # };
}
