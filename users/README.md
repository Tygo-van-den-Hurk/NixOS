# Dot Files for programs
This part of the repository contains all the configuration files for the programs that I use. Such as shells, window managers, browsers and much more. It is all run by home manager.

## Structure <!-- TODO : Add some text here... -->
As I haven't implemented this part yet, there isn't much to say about it yet. I'll add more documentation when it's done.

```
NixOS/
├── users/
│   ├── yourUserName/
│   │   ├── ...
│   │   └── default.nix
│   │ ...
│ ...
```
